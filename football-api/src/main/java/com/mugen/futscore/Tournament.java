package com.mugen.futscore;

import com.mugen.futscore.team.Team;
import lombok.Value;

import java.util.Set;

@Value(staticConstructor = "of")
public class Tournament {

    String name;
    Set<Team> teams;
    Set<Fixture> fixtures;

    public void addFixture(Fixture fixture) {
        if (!hasRegisteredTeams(fixture)) {
            throw new IllegalArgumentException("Only registered teams can have fixtures in the tournament");
        }
        this.fixtures.add(fixture);
    }

    private boolean hasRegisteredTeams(Fixture fixture) {
        return teams.contains(fixture.getHome().getTeam()) && teams.contains(fixture.getAway().getTeam());
    }

}
