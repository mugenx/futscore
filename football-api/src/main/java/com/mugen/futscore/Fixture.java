package com.mugen.futscore;

import com.mugen.futscore.team.Team;
import lombok.Builder;
import lombok.Value;

import java.time.LocalDateTime;

@Builder
@Value
public class Fixture {

    boolean settled;
    TeamState home;
    TeamState away;
    LocalDateTime saveDate;

    @Value(staticConstructor = "of")
    public static class TeamState {

        Team team;
        Integer score;

    }

}
