package com.mugen.futscore.team;

import com.mugen.futscore.Player;

import java.util.List;

public interface Team {

    String getName();

    List<Player> getPlayers();

}
