package com.mugen.futscore.team;

import com.mugen.futscore.Player;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;
import lombok.experimental.SuperBuilder;

import java.util.List;

@SuperBuilder
@Getter
@EqualsAndHashCode
@ToString
public abstract class AbstractTeam implements Team {

    String name;
    List<Player> players;

}
