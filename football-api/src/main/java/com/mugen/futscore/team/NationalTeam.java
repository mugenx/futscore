package com.mugen.futscore.team;

import com.mugen.futscore.Player;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import lombok.experimental.SuperBuilder;

@SuperBuilder
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class NationalTeam extends AbstractTeam {

    public void callUp(Player player) {
        throw new UnsupportedOperationException("NOT IMPLEMENTED");
    }

}
