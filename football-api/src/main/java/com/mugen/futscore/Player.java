package com.mugen.futscore;

import lombok.Value;

@Value(staticConstructor = "of")
public class Player {

    String firstName;
    String lastName;

}
