package com.mugen.futscore.sort;

import com.mugen.futscore.Fixture;
import com.mugen.futscore.FixtureSortFactory;
import com.mugen.futscore.Player;
import com.mugen.futscore.ScoreBoard;
import com.mugen.futscore.team.NationalTeam;
import com.mugen.futscore.team.Team;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.time.LocalDateTime;
import java.time.Month;
import java.util.Comparator;
import java.util.List;
import java.util.Set;
import java.util.stream.Stream;

import static com.mugen.futscore.FixtureSortFactory.FixtureSortType.SORT_BY_TOTAL_SCORE_AND_SAVED_DATE;
import static org.assertj.core.api.Assertions.assertThat;

class ScoreBoardTest {

    @ParameterizedTest(name = "{index} {0}")
    @MethodSource("createSortTestParams")
    void sortTest(String testedFeature,
                  Set<Team> participatingTeams,
                  List<Fixture> fixturesToAdd,
                  List<Fixture> expectedScores) {
        Comparator<Fixture> fixtureComparator = FixtureSortFactory.getComparator(SORT_BY_TOTAL_SCORE_AND_SAVED_DATE);
        ScoreBoard scoreBoard = new ScoreBoard("World Cup", participatingTeams, fixtureComparator);

        fixturesToAdd.forEach(scoreBoard::addFixture);

        assertThat(scoreBoard.getScores()).containsExactly(expectedScores.toArray(new Fixture[0]));
    }

    public static Stream<Arguments> createSortTestParams() {
        NationalTeam england = NationalTeam.builder()
                .name("England")
                .players(List.of(Player.of("Harry", "Kane"), Player.of("Kyle", "Walker")))
                .build();
        NationalTeam italy = NationalTeam.builder()
                .name("Italy")
                .players(List.of(Player.of("Federico", "Chiesa")))
                .build();
        NationalTeam spain = NationalTeam.builder()
                .name("(S)pain")
                .players(List.of(Player.of("Pedri", "Gonzalez")))
                .build();

        Fixture semis1 = Fixture.builder()
                .home(Fixture.TeamState.of(spain, 1))
                .away(Fixture.TeamState.of(italy, 1))
                .saveDate(LocalDateTime.of(2021, Month.JUNE, 1, 1, 1, 1))
                .settled(true)
                .build();
        Fixture semis2 = Fixture.builder()
                .home(Fixture.TeamState.of(spain, 1))
                .away(Fixture.TeamState.of(england, 1))
                .saveDate(LocalDateTime.of(2021, Month.JULY, 1, 1, 1, 1))
                .settled(true)
                .build();
        Fixture finals = Fixture.builder()
                .home(Fixture.TeamState.of(england, 1))
                .away(Fixture.TeamState.of(italy, 0))
                .saveDate(LocalDateTime.of(2021, Month.JULY, 1, 1, 1, 1))
                .settled(true)
                .build();

        return Stream.of(
                Arguments.of(
                        "Sort fixtures by total score",
                        Set.of(spain, italy, england),
                        List.of(finals, semis1),
                        List.of(semis1, finals)
                ),
                Arguments.of(
                        "Sort fixtures by added date",
                        Set.of(spain, italy, england),
                        List.of(semis1, semis2),
                        List.of(semis2, semis1)
                ),
                Arguments.of(
                        "Sort fixtures by total score and then added date",
                        Set.of(spain, italy, england),
                        List.of(semis1, finals, semis2),
                        List.of(semis2, semis1, finals)
                )
        );
    }


}