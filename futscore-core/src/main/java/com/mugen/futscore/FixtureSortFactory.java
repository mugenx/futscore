package com.mugen.futscore;

import java.util.Comparator;

import static com.mugen.futscore.FixtureSortFactory.FixtureSortType.SORT_BY_SAVED_DATE;
import static com.mugen.futscore.FixtureSortFactory.FixtureSortType.SORT_BY_TOTAL_SCORE;

public class FixtureSortFactory {

    public enum FixtureSortType {

        SORT_BY_TOTAL_SCORE_AND_SAVED_DATE,
        SORT_BY_TOTAL_SCORE,
        SORT_BY_SAVED_DATE

    }

    public static Comparator<Fixture> getComparator(FixtureSortType sortType) {
        // If there are more than 3 types and things get out of hand, we can use an EnumMap with sortType as key and Comparator as values.
        switch (sortType) {
            case SORT_BY_TOTAL_SCORE_AND_SAVED_DATE:
                return getComparator(SORT_BY_TOTAL_SCORE).thenComparing(getComparator(SORT_BY_SAVED_DATE));
            case SORT_BY_TOTAL_SCORE:
                return Comparator.comparing(FixtureSortFactory::getFixtureTotalScore).reversed();
            default:
            case SORT_BY_SAVED_DATE:
                return Comparator.comparing(Fixture::getSaveDate).reversed();
        }
    }

    private static int getFixtureTotalScore(Fixture fixture) {
        return fixture.getHome().getScore() + fixture.getAway().getScore();
    }

}
