package com.mugen.futscore;

import com.mugen.futscore.team.Team;
import lombok.experimental.Delegate;

import java.util.Comparator;
import java.util.Set;
import java.util.TreeSet;

/**
 * ScoreBoard is a class that provides sorted info about fixtures.
 */
public class ScoreBoard {

    @Delegate
    private final Tournament tournament;

    public ScoreBoard(String tournamentName,
                      Set<Team> teams,
                      Comparator<Fixture> fixtureComparator) {
        this.tournament = Tournament.of(tournamentName, teams, new TreeSet<>(fixtureComparator));
    }

    /**
     * @return Sorted list of fixtures in the score board
     */
    public Set<Fixture> getScores() {
        return tournament.getFixtures();
    }

}
